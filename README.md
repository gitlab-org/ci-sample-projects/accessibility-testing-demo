# Accessibility Testing Demo

This is a demo of the [GitLab Accessibility Testing](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html) feature.

We are only running accessibility testing on review apps in this demo.

View [the currently open MR](https://gitlab.com/gitlab-org/ci-sample-projects/accessibility-testing-demo/-/merge_requests/3) to see a demo of the accessibility report.

## Repository Structure

Accessibility Testing requires a static site and a deployed environment in order to work.
This repository uses the following tools to achieve that.

### Gatsby

This project uses [Gatsby](https://www.gatsbyjs.com/) to build a static site. The current site is the default one
automatically generated when you initialize a new project.

### Surge.sh

In order to deploy review apps, this project is using [Surge.sh](https://surge.sh/) to easily host a static site.
